package org.example;

import static org.example.Constant.GRAPH;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BreadthFirstSearch {

    public List<Integer> runBreadthFirstSearch(int startStationId, int endStationId) { // поиск в ширину
        Queue<Integer> queue = new LinkedList<>(); // создаем очередь для вершин, которые нужно посетить
        Set<Integer> visited = new HashSet<>(); // создаем set для вершин, которые посетили
        Map<Integer, Integer> path = new HashMap<>(); // создали словарь для хранения пути. ключ -- узел, а значение -- предшественник

        queue.add(startStationId); // добавляем в очередь для посещения начальную вершину, она далее станет текущей
        visited.add(startStationId); //добавляем в очередь посещенных начальный узел
        path.put(startStationId, null); // добавляем в предшественника null -- start -- начальный узел

        while (!queue.isEmpty()) {
            int current = queue.remove();//удаляем из очереди текущую вершину, которая находится первой в очереди, то есть на нужном нам уровне

            if (current == endStationId) { //если текущая -- конечный узел, то восстанавливаем путь
                return reconstructPath(path, endStationId);
            }

            for (int neighbor : GRAPH.getConnections(current)) { //берем все станции, до которых можем добраться от текущей -- текущий уровень графа
                if (!visited.contains(neighbor)) { //если данного соседа нет в списке посещенных, то
                    queue.add(neighbor); //добавляем его в очередь на посещение
                    visited.add(neighbor); // cразу добавим в посещенный сет
                    path.put(neighbor, current); //обновляем путь
                }
            }
        }
        return null; // Путь не найден
    }

    private List<Integer> reconstructPath(Map<Integer, Integer> path, int endStationId) { // метод восстановления пути
        List<Integer> route = new ArrayList<>(); // путь от конечной станции до начальной, далее развернем его
        Integer current = endStationId; // текущая вершина -- вершина назначения

        while (current != null) { //пока не дошли до null (то есть первая вершина -- вершина , из которой начинали
            route.add(current); // добавляем в маршрут текущую станцию
            current = path.get(current); // обновляем текущую станцию
        }

        Collections.reverse(route); //разворачиваем список
        return route; //возвращаем путь
    }
}
