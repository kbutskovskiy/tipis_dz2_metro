package org.example;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите название станции , с которой начинается маршрут: ");
        String firstVertex = scanner.nextLine();
        System.out.println("Введите номер ветки: ");
        String firstLine = scanner.nextLine();
        System.out.println("Введите название станции , на которой заканчивается маршрут: ");
        String secondVertex = scanner.nextLine();
        System.out.println("Введите номер ветки: ");
        String secondLine = scanner.nextLine();
        Station firstStation = new Station(firstLine, firstVertex); // cоздаем экземпляр первой станции
        Station secondStation = new Station(secondLine, secondVertex); // cоздаем экземпляр второй станции
        Integer firstIndex = -1; // делаем индексы -1
        Integer secondIndex = -1;
        for (Map.Entry<Integer, Station> entry : Constant.STATIONS.entrySet()) { //ищем индекс станции в Map
            if (firstStation.equals(entry.getValue())) { //мы переопределили equals , поэтому он будет работать корректно. если нашли такой же класс
                firstIndex = entry.getKey(); // то достаем из map ключ, то есть идентификатор
            }
            if (secondStation.equals(entry.getValue())) {
                secondIndex = entry.getKey();
            }
            if (firstIndex >= 0 && secondIndex >= 0) { //нашли оба идентификатора
                break;
            }
        }
        if (firstIndex == -1 || secondIndex == -1) { //не нашли какой-либо идентификатор
            System.out.println("Неправильно ввели название или номер ветки");
            return;
        }
        BreadthFirstSearch breadthFirstSearch = new BreadthFirstSearch();
        List<Integer> path = breadthFirstSearch.runBreadthFirstSearch(firstIndex, secondIndex); //запускаем алгоритм поиска в ширину
        System.out.print("Путь:\n" + Constant.STATIONS.get(path.get(0)).getName());
        Integer currentVertex = path.get(0);
        Integer lastVertex = path.get(path.size()-1);
        Integer prevVertex = currentVertex; //выписываем путь
        for (Integer station : path) {
            if (!Objects.equals(Constant.STATIONS.get(station).getLine(),
                    Constant.STATIONS.get(currentVertex).getLine())) {
                System.out.print(" -> " + Constant.STATIONS.get(prevVertex).getName() + " пересадка на линию: " + Constant.LINES.get(Constant.STATIONS.get(station).getLine()));
                System.out.print(", на станцию " + Constant.STATIONS.get(station).getName());
                currentVertex = station;
            }
            if (station == lastVertex) {
                System.out.print(" -> " + Constant.STATIONS.get(station).getName());
            }
            prevVertex = station;
        }
    }
}