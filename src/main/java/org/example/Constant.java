package org.example;

import java.util.Map;
import java.util.HashMap;

public class Constant {
    public static final Map<String, String> LINES; // мапа всех линий в метро
    public static final Map<Integer, Station> STATIONS; // мапа всех станций метро
    public static final MetroGraph GRAPH; // список смежности станций метро, включая пересадки

    static {
        //заполянем линии метро
        LINES = new HashMap<>();
        LINES.put("1", "Сокольническая линия");
        LINES.put("2", "Замоскворецкая линия");
        LINES.put("3", "Арбатско-Покровская линия");
        LINES.put("4", "Филевская линия");
        LINES.put("5", "Кольцевая линия");
        LINES.put("6", "Калужско-Рижская линия");
        LINES.put("7", "Таганско-Краснопресненская линия");
        LINES.put("8", "Калининско-Солнцевская линия");
        LINES.put("9", "Серпуховско-Тимирязевская линия");
        LINES.put("10", "Люблинско-Дмитровская линия");
        LINES.put("11", "БКЛ");
        LINES.put("12", "Бутовская линия");
        LINES.put("13", "Московский монорельс");
        LINES.put("14", "Московское центральное кольцо");
        LINES.put("15", "Некрасовская линия");
        LINES.put("D1", "Первый диаметр");
        LINES.put("D2", "Второй диаметр");
        LINES.put("D3", "Третий диаметр");
        LINES.put("D4", "Четвертый диаметр");

        //заполняем все станции метро, мцд , мцк
        STATIONS = new HashMap<>();
        STATIONS.put(0, new Station("1", "Бульвар Рокоссовского"));
        STATIONS.put(1, new Station("1", "Черкизовская"));
        STATIONS.put(2, new Station("1", "Преображенская площадь"));
        STATIONS.put(3, new Station("1", "Сокольники"));
        STATIONS.put(4, new Station("1", "Красносельская"));
        STATIONS.put(5, new Station("1", "Комсомольская"));
        STATIONS.put(6, new Station("1", "Красные ворота"));
        STATIONS.put(7, new Station("1", "Чистые пруды"));
        STATIONS.put(8, new Station("1", "Лубянка"));
        STATIONS.put(9, new Station("1", "Охотный ряд"));
        STATIONS.put(10, new Station("1", "Библиотека имени Ленина"));
        STATIONS.put(11, new Station("1", "Кропоткинская"));
        STATIONS.put(12, new Station("1", "Парк культуры"));
        STATIONS.put(13, new Station("1", "Фрунзенская"));
        STATIONS.put(14, new Station("1", "Спортивная"));
        STATIONS.put(15, new Station("1", "Воробьёвы горы"));
        STATIONS.put(16, new Station("1", "Университет"));
        STATIONS.put(17, new Station("1", "Проспект Вернадского"));
        STATIONS.put(18, new Station("1", "Юго-Западная"));
        STATIONS.put(19, new Station("1", "Тропарёво"));
        STATIONS.put(20, new Station("1", "Румянцево"));
        STATIONS.put(21, new Station("1", "Саларьево"));
        STATIONS.put(22, new Station("1", "Филатов Луг"));
        STATIONS.put(23, new Station("1", "Прокшино"));
        STATIONS.put(24, new Station("1", "Ольховая"));
        STATIONS.put(25, new Station("1", "Коммунарка"));

        STATIONS.put(26, new Station("2", "Ховрино"));
        STATIONS.put(27, new Station("2", "Беломорская"));
        STATIONS.put(28, new Station("2", "Речной вокзал"));
        STATIONS.put(29, new Station("2", "Водный стадион"));
        STATIONS.put(30, new Station("2", "Войковская"));
        STATIONS.put(31, new Station("2", "Сокол"));
        STATIONS.put(32, new Station("2", "Аэропорт"));
        STATIONS.put(33, new Station("2", "Динамо"));
        STATIONS.put(34, new Station("2", "Белорусская"));
        STATIONS.put(35, new Station("2", "Маяковская"));
        STATIONS.put(36, new Station("2", "Тверская"));
        STATIONS.put(37, new Station("2", "Театральная"));
        STATIONS.put(38, new Station("2", "Новокузнецкая"));
        STATIONS.put(39, new Station("2", "Павелецкая"));
        STATIONS.put(40, new Station("2", "Автозаводская"));
        STATIONS.put(41, new Station("2", "Технопарк"));
        STATIONS.put(42, new Station("2", "Коломенская"));
        STATIONS.put(43, new Station("2", "Каширская"));
        STATIONS.put(44, new Station("2", "Кантемировская"));
        STATIONS.put(45, new Station("2", "Царицыно"));
        STATIONS.put(46, new Station("2", "Орехово"));
        STATIONS.put(47, new Station("2", "Домодедовская"));
        STATIONS.put(48, new Station("2", "Красногвардейская"));
        STATIONS.put(49, new Station("2", "Алма-Атинская"));

        STATIONS.put(50, new Station("3", "Щелковская"));
        STATIONS.put(51, new Station("3", "Первомайская"));
        STATIONS.put(52, new Station("3", "Измайловская"));
        STATIONS.put(53, new Station("3", "Партизанская"));
        STATIONS.put(54, new Station("3", "Семёновская"));
        STATIONS.put(55, new Station("3", "Электрозаводская"));
        STATIONS.put(56, new Station("3", "Бауманская"));
        STATIONS.put(57, new Station("3", "Курская"));
        STATIONS.put(58, new Station("3", "Площадь Революции"));
        STATIONS.put(59, new Station("3", "Арбатская"));
        STATIONS.put(60, new Station("3", "Смоленская"));
        STATIONS.put(61, new Station("3", "Киевская"));
        STATIONS.put(62, new Station("3", "Парк Победы"));
        STATIONS.put(63, new Station("3", "Славянский бульвар"));
        STATIONS.put(64, new Station("3", "Кунцевская"));
        STATIONS.put(65, new Station("3", "Молодёжная"));
        STATIONS.put(66, new Station("3", "Крылатское"));
        STATIONS.put(67, new Station("3", "Строгино"));
        STATIONS.put(68, new Station("3", "Мякинино"));
        STATIONS.put(69, new Station("3", "Волоколамская"));
        STATIONS.put(70, new Station("3", "Митино"));
        STATIONS.put(71, new Station("3", "Пятницкое шоссе"));

        STATIONS.put(72, new Station("4", "Кунцевская"));
        STATIONS.put(73, new Station("4", "Пионерская"));
        STATIONS.put(74, new Station("4", "Филёвский парк"));
        STATIONS.put(75, new Station("4", "Багратионовская"));
        STATIONS.put(76, new Station("4", "Фили"));
        STATIONS.put(77, new Station("4", "Кутузовская"));
        STATIONS.put(78, new Station("4", "Студенческая"));
        STATIONS.put(80, new Station("4", "Смоленская"));
        STATIONS.put(81, new Station("4", "Арбатская"));
        STATIONS.put(82, new Station("4", "Александровский сад"));

        STATIONS.put(83, new Station("4A", "Киевская"));
        STATIONS.put(84, new Station("4A", "Выставочная"));
        STATIONS.put(85, new Station("4A", "Международная"));

        STATIONS.put(86, new Station("5", "Киевская"));
        STATIONS.put(87, new Station("5", "Парк культуры"));
        STATIONS.put(88, new Station("5", "Октябрьская"));
        STATIONS.put(89, new Station("5", "Добрынинская"));
        STATIONS.put(90, new Station("5", "Павелецкая"));
        STATIONS.put(91, new Station("5", "Таганская"));
        STATIONS.put(92, new Station("5", "Курская"));
        STATIONS.put(93, new Station("5", "Комсомольская"));
        STATIONS.put(94, new Station("5", "Проспект Мира"));
        STATIONS.put(95, new Station("5", "Новослободская"));
        STATIONS.put(96, new Station("5", "Белорусская"));
        STATIONS.put(97, new Station("5", "Краснопресненская"));

        STATIONS.put(98, new Station("6", "Медведково"));
        STATIONS.put(99, new Station("6", "Бабушкинская"));
        STATIONS.put(100, new Station("6", "Свиблово"));
        STATIONS.put(101, new Station("6", "Ботанический сад"));
        STATIONS.put(102, new Station("6", "ВДНХ"));
        STATIONS.put(103, new Station("6", "Алексеевская"));
        STATIONS.put(104, new Station("6", "Рижская"));
        STATIONS.put(105, new Station("6", "Проспект Мира"));
        STATIONS.put(106, new Station("6", "Сухаревская"));
        STATIONS.put(107, new Station("6", "Тургеневская"));
        STATIONS.put(108, new Station("6", "Китай-город"));
        STATIONS.put(109, new Station("6", "Третьяковская"));
        STATIONS.put(110, new Station("6", "Октябрьская"));
        STATIONS.put(111, new Station("6", "Шаболовская"));
        STATIONS.put(112, new Station("6", "Ленинский проспект"));
        STATIONS.put(113, new Station("6", "Академическая"));
        STATIONS.put(114, new Station("6", "Профсоюзная"));
        STATIONS.put(115, new Station("6", "Новые Черёмушки"));
        STATIONS.put(116, new Station("6", "Калужская"));
        STATIONS.put(117, new Station("6", "Беляево"));
        STATIONS.put(118, new Station("6", "Коньково"));
        STATIONS.put(119, new Station("6", "Тёплый Стан"));
        STATIONS.put(120, new Station("6", "Ясенево"));
        STATIONS.put(121, new Station("6", "Новоясеневская"));

        STATIONS.put(122, new Station("7", "Планерная"));
        STATIONS.put(123, new Station("7", "Сходненская"));
        STATIONS.put(124, new Station("7", "Тушинская"));
        STATIONS.put(125, new Station("7", "Щукинская"));
        STATIONS.put(126, new Station("7", "Октябрьское поле"));
        STATIONS.put(127, new Station("7", "Полежаевская"));
        STATIONS.put(128, new Station("7", "Беговая"));
        STATIONS.put(129, new Station("7", "Улица 1905 года"));
        STATIONS.put(130, new Station("7", "Баррикадная"));
        STATIONS.put(131, new Station("7", "Пушкинская"));
        STATIONS.put(132, new Station("7", "Кузнецкий мост"));
        STATIONS.put(133, new Station("7", "Китай-город"));
        STATIONS.put(134, new Station("7", "Таганская"));
        STATIONS.put(135, new Station("7", "Пролетарская"));
        STATIONS.put(136, new Station("7", "Волгоградский проспект"));
        STATIONS.put(137, new Station("7", "Текстильщики"));
        STATIONS.put(138, new Station("7", "Кузьминки"));
        STATIONS.put(139, new Station("7", "Рязанский проспект"));
        STATIONS.put(140, new Station("7", "Выхино"));
        STATIONS.put(141, new Station("7", "Лермонтовский проспект"));
        STATIONS.put(142, new Station("7", "Жулебино"));
        STATIONS.put(143, new Station("7", "Котельники"));

        STATIONS.put(144, new Station("8", "Новокосино"));
        STATIONS.put(145, new Station("8", "Новогиреево"));
        STATIONS.put(146, new Station("8", "Перово"));
        STATIONS.put(147, new Station("8", "Шоссе Энтузиастов"));
        STATIONS.put(148, new Station("8", "Авиамоторная"));
        STATIONS.put(149, new Station("8", "Площадь Ильича"));
        STATIONS.put(150, new Station("8", "Марксистская"));
        STATIONS.put(151, new Station("8", "Третьяковская"));

        STATIONS.put(152, new Station("8А", "Деловой центр"));
        STATIONS.put(153, new Station("8А", "Парк Победы"));
        STATIONS.put(154, new Station("8А", "Минская"));
        STATIONS.put(155, new Station("8А", "Ломоносовский проспект"));
        STATIONS.put(156, new Station("8А", "Раменки"));
        STATIONS.put(157, new Station("8А", "Мичуринский проспект"));
        STATIONS.put(158, new Station("8А", "Озёрная"));
        STATIONS.put(159, new Station("8А", "Говорово"));
        STATIONS.put(160, new Station("8А", "Солнцево"));
        STATIONS.put(161, new Station("8А", "Боровское шоссе"));
        STATIONS.put(162, new Station("8А", "Новопеределкино"));
        STATIONS.put(163, new Station("8А", "Рассказовка"));
        STATIONS.put(164, new Station("8А", "Пыхтино"));
        STATIONS.put(165, new Station("8А", "Аэропорт Внуково"));


        STATIONS.put(172, new Station("9", "Алтуфьево"));
        STATIONS.put(173, new Station("9", "Бибирево"));
        STATIONS.put(174, new Station("9", "Отрадное"));
        STATIONS.put(175, new Station("9", "Владыкино"));
        STATIONS.put(176, new Station("9", "Петровско-Разумовская"));
        STATIONS.put(177, new Station("9", "Тимирязевская"));
        STATIONS.put(178, new Station("9", "Дмитровская"));
        STATIONS.put(179, new Station("9", "Савёловская"));
        STATIONS.put(180, new Station("9", "Менделеевская"));
        STATIONS.put(181, new Station("9", "Цветной бульвар"));
        STATIONS.put(182, new Station("9", "Чеховская"));
        STATIONS.put(183, new Station("9", "Боровицкая"));
        STATIONS.put(184, new Station("9", "Полянка"));
        STATIONS.put(185, new Station("9", "Серпуховская"));
        STATIONS.put(186, new Station("9", "Тульская"));
        STATIONS.put(187, new Station("9", "Нагатинская"));
        STATIONS.put(188, new Station("9", "Нагорная"));
        STATIONS.put(189, new Station("9", "Нахимовский проспект"));
        STATIONS.put(190, new Station("9", "Севастопольская"));
        STATIONS.put(191, new Station("9", "Чертановская"));
        STATIONS.put(192, new Station("9", "Южная"));
        STATIONS.put(193, new Station("9", "Пражская"));
        STATIONS.put(194, new Station("9", "Улица академика Янгеля"));
        STATIONS.put(195, new Station("9", "Аннино"));
        STATIONS.put(196, new Station("9", "Бульвар Дмитрия Донского"));

        STATIONS.put(197, new Station("10", "Физтех"));
        STATIONS.put(198, new Station("10", "Лианозово"));
        STATIONS.put(199, new Station("10", "Яхромская"));
        STATIONS.put(200, new Station("10", "Селигерская"));
        STATIONS.put(201, new Station("10", "Верхние Лихоборы"));
        STATIONS.put(202, new Station("10", "Окружная"));
        STATIONS.put(203, new Station("10", "Петровско-Разумовская"));
        STATIONS.put(204, new Station("10", "Фонвизинская"));
        STATIONS.put(205, new Station("10", "Бутырская"));
        STATIONS.put(206, new Station("10", "Марьина Роща"));
        STATIONS.put(207, new Station("10", "Достоевская"));
        STATIONS.put(208, new Station("10", "Трубная"));
        STATIONS.put(209, new Station("10", "Сретенский бульвар"));
        STATIONS.put(210, new Station("10", "Чкаловская"));
        STATIONS.put(211, new Station("10", "Римская"));
        STATIONS.put(212, new Station("10", "Крестьянская застава"));
        STATIONS.put(213, new Station("10", "Дубровка"));
        STATIONS.put(214, new Station("10", "Кожуховская"));
        STATIONS.put(215, new Station("10", "Печатники"));
        STATIONS.put(216, new Station("10", "Волжская"));
        STATIONS.put(217, new Station("10", "Люблино"));
        STATIONS.put(218, new Station("10", "Братиславская"));
        STATIONS.put(219, new Station("10", "Марьино"));
        STATIONS.put(220, new Station("10", "Борисово"));
        STATIONS.put(221, new Station("10", "Шипиловская"));
        STATIONS.put(222, new Station("10", "Зябликово"));

        STATIONS.put(223, new Station("11", "Хорошевская"));
        STATIONS.put(224, new Station("11", "Петровский парк"));
        STATIONS.put(225, new Station("11", "Савеловская"));
        STATIONS.put(226, new Station("11", "Марьина роща"));
        STATIONS.put(227, new Station("11", "Рижская"));
        STATIONS.put(228, new Station("11", "Сокольники"));
        STATIONS.put(229, new Station("11", "Электрозаводская"));
        STATIONS.put(230, new Station("11", "Лефортово"));
        STATIONS.put(231, new Station("11", "Авиамоторная"));
        STATIONS.put(232, new Station("11", "Нижегородская"));
        STATIONS.put(233, new Station("11", "Текстильщики"));
        STATIONS.put(234, new Station("11", "Печатники"));
        STATIONS.put(235, new Station("11", "Нагатинский затон"));
        STATIONS.put(236, new Station("11", "Кленовый бульвар"));
        STATIONS.put(237, new Station("11", "Каширская"));
        STATIONS.put(238, new Station("11", "Варшавская"));
        STATIONS.put(239, new Station("11", "Каховская"));
        STATIONS.put(240, new Station("11", "Зюзино"));
        STATIONS.put(241, new Station("11", "Воронцовская"));
        STATIONS.put(242, new Station("11", "Новаторская"));
        STATIONS.put(243, new Station("11", "Проспект Вернадского"));
        STATIONS.put(244, new Station("11", "Мичуринский проспект"));
        STATIONS.put(245, new Station("11", "Аминьевская"));
        STATIONS.put(246, new Station("11", "Давыдково"));
        STATIONS.put(247, new Station("11", "Кунцевская"));
        STATIONS.put(248, new Station("11", "Терехово"));
        STATIONS.put(249, new Station("11", "Мневники"));
        STATIONS.put(250, new Station("11", "Народное ополчение"));

        STATIONS.put(435, new Station("11А", "ЦСКА"));
        STATIONS.put(251, new Station("11А", "Хорошевская"));
        STATIONS.put(252, new Station("11А", "Шелепиха"));
        STATIONS.put(253, new Station("11А", "Деловой центр"));

        STATIONS.put(254, new Station("12", "Бунинская аллея"));
        STATIONS.put(255, new Station("12", "Улица Горчакова"));
        STATIONS.put(256, new Station("12", "Бульвар Адмирала Ушакова"));
        STATIONS.put(257, new Station("12", "Улица Скобелевская"));
        STATIONS.put(258, new Station("12", "Улица Старокачаловская"));
        STATIONS.put(259, new Station("12", "Лесопарковая"));
        STATIONS.put(260, new Station("12", "Битцевский парк"));

        STATIONS.put(261, new Station("14", "Окружная"));
        STATIONS.put(262, new Station("14", "Владыкино"));
        STATIONS.put(263, new Station("14", "Ботанический сад"));
        STATIONS.put(264, new Station("14", "Ростокино"));
        STATIONS.put(265, new Station("14", "Белокаменная"));
        STATIONS.put(266, new Station("14", "Бульвар Рокоссовского"));
        STATIONS.put(267, new Station("14", "Локомотив"));
        STATIONS.put(268, new Station("14", "Измайлово"));
        STATIONS.put(269, new Station("14", "Соколиная гора"));
        STATIONS.put(436, new Station("14", "Шоссе энтузиастов"));
        STATIONS.put(270, new Station("14", "Андроновка"));
        STATIONS.put(271, new Station("14", "Нижегородская"));
        STATIONS.put(272, new Station("14", "Новохохловская"));
        STATIONS.put(273, new Station("14", "Угрешская"));
        STATIONS.put(274, new Station("14", "Дубровка"));
        STATIONS.put(275, new Station("14", "Автозаводская"));
        STATIONS.put(276, new Station("14", "ЗИЛ"));
        STATIONS.put(277, new Station("14", "Верхние Котлы"));
        STATIONS.put(278, new Station("14", "Крымская"));
        STATIONS.put(279, new Station("14", "Площадь Гагарина"));
        STATIONS.put(280, new Station("14", "Лужники"));
        STATIONS.put(281, new Station("14", "Кутузовская"));
        STATIONS.put(282, new Station("14", "Деловой центр"));
        STATIONS.put(283, new Station("14", "Шелепиха"));
        STATIONS.put(284, new Station("14", "Хорошёво"));
        STATIONS.put(285, new Station("14", "Зорге"));
        STATIONS.put(286, new Station("14", "Панфиловская"));
        STATIONS.put(287, new Station("14", "Стрешнево"));
        STATIONS.put(288, new Station("14", "Балтийская"));
        STATIONS.put(289, new Station("14", "Коптево"));
        STATIONS.put(290, new Station("14", "Лихоборы"));

        STATIONS.put(291, new Station("15", "Нижегородская"));
        STATIONS.put(292, new Station("15", "Стахановская"));
        STATIONS.put(293, new Station("15", "Окская"));
        STATIONS.put(294, new Station("15", "Юго-Восточная"));
        STATIONS.put(295, new Station("15", "Косино"));
        STATIONS.put(296, new Station("15", "Улица Дмитриевского"));
        STATIONS.put(297, new Station("15", "Лухмановская"));
        STATIONS.put(298, new Station("15", "Некрасовка"));

        STATIONS.put(299, new Station("D1", "Лобня"));
        STATIONS.put(300, new Station("D1", "Шереметьевская"));
        STATIONS.put(301, new Station("D1", "Хлебниково"));
        STATIONS.put(302, new Station("D1", "Водники"));
        STATIONS.put(303, new Station("D1", "Долгопрудная"));
        STATIONS.put(304, new Station("D1", "Новодачная"));
        STATIONS.put(305, new Station("D1", "Марк"));
        STATIONS.put(306, new Station("D1", "Лианозово"));
        STATIONS.put(307, new Station("D1", "Бескудниково"));
        STATIONS.put(308, new Station("D1", "Дегунино"));
        STATIONS.put(309, new Station("D1", "Окружная"));
        STATIONS.put(310, new Station("D1", "Тимирязевская"));
        STATIONS.put(311, new Station("D1", "Савёловская"));
        STATIONS.put(312, new Station("D1", "Белорусская"));
        STATIONS.put(313, new Station("D1", "Беговая"));
        STATIONS.put(314, new Station("D1", "Тестовская"));
        STATIONS.put(315, new Station("D1", "Фили"));
        STATIONS.put(316, new Station("D1", "Славянский бульвар"));
        STATIONS.put(317, new Station("D1", "Кунцевская"));
        STATIONS.put(318, new Station("D1", "Рабочий Посёлок"));
        STATIONS.put(319, new Station("D1", "Сетунь"));
        STATIONS.put(320, new Station("D1", "Немчиновка"));
        STATIONS.put(321, new Station("D1", "Сколково"));
        STATIONS.put(322, new Station("D1", "Баковка"));
        STATIONS.put(323, new Station("D1", "Одинцово"));

        STATIONS.put(324, new Station("D2", "Нахабино"));
        STATIONS.put(325, new Station("D2", "Аникеевка"));
        STATIONS.put(326, new Station("D2", "Опалиха"));
        STATIONS.put(327, new Station("D2", "Красногорская"));
        STATIONS.put(328, new Station("D2", "Павшино"));
        STATIONS.put(329, new Station("D2", "Пенягино"));
        STATIONS.put(330, new Station("D2", "Волоколамская"));
        STATIONS.put(331, new Station("D2", "Трикотажная"));
        STATIONS.put(332, new Station("D2", "Тушинская"));
        STATIONS.put(333, new Station("D2", "Щукинская"));
        STATIONS.put(334, new Station("D2", "Стрешнево"));
        STATIONS.put(335, new Station("D2", "Красный Балтиец"));
        STATIONS.put(336, new Station("D2", "Гражданская"));
        STATIONS.put(337, new Station("D2", "Дмитровская"));
        STATIONS.put(338, new Station("D2", "Марьина Роща"));
        STATIONS.put(339, new Station("D2", "Рижская"));
        STATIONS.put(340, new Station("D2", "Площадь трех вокзалов"));
        STATIONS.put(341, new Station("D2", "Курская"));
        STATIONS.put(342, new Station("D2", "Москва-Товарная"));
        STATIONS.put(343, new Station("D2", "Калитники"));
        STATIONS.put(344, new Station("D2", "Новохохловская"));
        STATIONS.put(345, new Station("D2", "Текстильщики"));
        STATIONS.put(346, new Station("D2", "Печатники"));
        STATIONS.put(347, new Station("D2", "Люблино"));
        STATIONS.put(348, new Station("D2", "Депо"));
        STATIONS.put(349, new Station("D2", "Перерва"));
        STATIONS.put(350, new Station("D2", "Курьяново"));
        STATIONS.put(351, new Station("D2", "Новохохловская"));
        STATIONS.put(352, new Station("D2", "Москворечье"));
        STATIONS.put(353, new Station("D2", "Царицыно"));
        STATIONS.put(354, new Station("D2", "Покровское"));
        STATIONS.put(355, new Station("D2", "Красный Строитель"));
        STATIONS.put(356, new Station("D2", "Битца"));
        STATIONS.put(357, new Station("D2", "Бутово"));
        STATIONS.put(358, new Station("D2", "Щербинка"));
        STATIONS.put(359, new Station("D2", "Остафьево"));
        STATIONS.put(360, new Station("D2", "Силикатная"));
        STATIONS.put(361, new Station("D2", "Подольск"));

        STATIONS.put(362, new Station("D3", "Зеленоград-Крюково"));
        STATIONS.put(363, new Station("D3", "Фирсановская"));
        STATIONS.put(364, new Station("D3", "Сходня"));
        STATIONS.put(365, new Station("D3", "Подрезково"));
        STATIONS.put(366, new Station("D3", "Новоподрезково"));
        STATIONS.put(367, new Station("D3", "Молжаниново"));
        STATIONS.put(368, new Station("D3", "Химки"));
        STATIONS.put(369, new Station("D3", "Левобережная"));
        STATIONS.put(370, new Station("D3", "Ховрино"));
        STATIONS.put(371, new Station("D3", "Грачёвская"));
        STATIONS.put(372, new Station("D3", "Моссельмаш"));
        STATIONS.put(373, new Station("D3", "Лихоборы"));
        STATIONS.put(374, new Station("D3", "Петровско-Разумовская"));
        STATIONS.put(375, new Station("D3", "Останкино"));
        STATIONS.put(376, new Station("D3", "Электрозаводская"));
        STATIONS.put(377, new Station("D3", "Сортировочная"));
        STATIONS.put(378, new Station("D3", "Авиамоторная"));
        STATIONS.put(379, new Station("D3", "Андроновка"));
        STATIONS.put(380, new Station("D3", "Перово"));
        STATIONS.put(381, new Station("D3", "Плющево"));
        STATIONS.put(382, new Station("D3", "Вешняки"));
        STATIONS.put(383, new Station("D3", "Выхино"));
        STATIONS.put(384, new Station("D3", "Косино"));
        STATIONS.put(385, new Station("D3", "Ухтомская"));
        STATIONS.put(386, new Station("D3", "Люберцы"));
        STATIONS.put(387, new Station("D3", "Панки"));
        STATIONS.put(388, new Station("D3", "Томилино"));
        STATIONS.put(389, new Station("D3", "Красково"));
        STATIONS.put(390, new Station("D3", "Малаховка"));
        STATIONS.put(391, new Station("D3", "Удельная"));
        STATIONS.put(392, new Station("D3", "Быково"));
        STATIONS.put(393, new Station("D3", "Ильинская"));
        STATIONS.put(394, new Station("D3", "Отдых"));
        STATIONS.put(395, new Station("D3", "Кратово"));
        STATIONS.put(396, new Station("D3", "Есенинская"));
        STATIONS.put(397, new Station("D3", "Фабричная"));
        STATIONS.put(398, new Station("D3", "Раменское"));
        STATIONS.put(399, new Station("D3", "Ипподром"));

        STATIONS.put(400, new Station("D4", "Апрелевка"));
        STATIONS.put(401, new Station("D4", "Победа"));
        STATIONS.put(402, new Station("D4", "Крёкшино"));
        STATIONS.put(403, new Station("D4", "Санино"));
        STATIONS.put(404, new Station("D4", "Кокошкино"));
        STATIONS.put(405, new Station("D4", "Толстопальцево"));
        STATIONS.put(406, new Station("D4", "Лесной городок"));
        STATIONS.put(407, new Station("D4", "Внуково"));
        STATIONS.put(408, new Station("D4", "Мичуринец"));
        STATIONS.put(409, new Station("D4", "Переделкино"));
        STATIONS.put(410, new Station("D4", "Солнечная"));
        STATIONS.put(411, new Station("D4", "Мещерская"));
        STATIONS.put(412, new Station("D4", "Очаково"));
        STATIONS.put(413, new Station("D4", "Аминьевская"));
        STATIONS.put(414, new Station("D4", "Матвеевская"));
        STATIONS.put(415, new Station("D4", "Минская"));
        STATIONS.put(416, new Station("D4", "Поклонная"));
        STATIONS.put(417, new Station("D4", "Кутузовская"));
        STATIONS.put(418, new Station("D4", "Тестовская"));
        STATIONS.put(419, new Station("D4", "Белорусская"));
        STATIONS.put(420, new Station("D4", "Савёловская"));
        STATIONS.put(421, new Station("D4", "Марьина Роща"));
        STATIONS.put(422, new Station("D4", "Площадь трех вокзалов"));
        STATIONS.put(423, new Station("D4", "Курская"));
        STATIONS.put(424, new Station("D4", "Серп и Молот"));
        STATIONS.put(425, new Station("D4", "Нижегородская"));
        STATIONS.put(426, new Station("D4", "Чухлинка"));
        STATIONS.put(427, new Station("D4", "Кусково"));
        STATIONS.put(428, new Station("D4", "Новогиреево"));
        STATIONS.put(429, new Station("D4", "Реутов"));
        STATIONS.put(430, new Station("D4", "Никольское"));
        STATIONS.put(431, new Station("D4", "Салтыковская"));
        STATIONS.put(432, new Station("D4", "Кучино"));
        STATIONS.put(433, new Station("D4", "Ольгино"));
        STATIONS.put(434, new Station("D4", "Железнодорожная"));


        //добавляем соединения
        GRAPH = new MetroGraph();
        //Connections Line 1
        GRAPH.addConnection(0, 266);
        GRAPH.addConnection(1, 267);
        GRAPH.addConnection(3, 228);
        GRAPH.addConnection(5, 93);
        GRAPH.addConnection(5, 340);
        GRAPH.addConnection(5, 422);
        GRAPH.addConnection(7, 107);
        GRAPH.addConnection(7, 209);
        GRAPH.addConnection(8, 132);
        GRAPH.addConnection(9, 37);
        GRAPH.addConnection(9, 58);
        GRAPH.addConnection(10, 82);
        GRAPH.addConnection(10, 59);
        GRAPH.addConnection(10, 183);
        GRAPH.addConnection(12, 87);
        GRAPH.addConnection(14, 280);
        GRAPH.addConnection(17, 243);
        GRAPH.addConnection(0, 1);
        GRAPH.addConnection(1, 0);
        GRAPH.addConnection(1, 2);
        GRAPH.addConnection(2, 1);
        GRAPH.addConnection(2, 3);
        GRAPH.addConnection(3, 2);
        GRAPH.addConnection(3, 4);
        GRAPH.addConnection(4, 3);
        GRAPH.addConnection(4, 5);
        GRAPH.addConnection(5, 4);
        GRAPH.addConnection(5, 6);
        GRAPH.addConnection(6, 5);
        GRAPH.addConnection(6, 7);
        GRAPH.addConnection(7, 6);
        GRAPH.addConnection(7, 8);
        GRAPH.addConnection(8, 7);
        GRAPH.addConnection(8, 9);
        GRAPH.addConnection(9, 8);
        GRAPH.addConnection(9, 10);
        GRAPH.addConnection(10, 9);
        GRAPH.addConnection(10, 11);
        GRAPH.addConnection(11, 10);
        GRAPH.addConnection(11, 12);
        GRAPH.addConnection(12, 11);
        GRAPH.addConnection(12, 13);
        GRAPH.addConnection(13, 12);
        GRAPH.addConnection(13, 14);
        GRAPH.addConnection(14, 13);
        GRAPH.addConnection(14, 15);
        GRAPH.addConnection(15, 14);
        GRAPH.addConnection(15, 16);
        GRAPH.addConnection(16, 15);
        GRAPH.addConnection(16, 17);
        GRAPH.addConnection(17, 16);
        GRAPH.addConnection(17, 18);
        GRAPH.addConnection(18, 17);
        GRAPH.addConnection(18, 19);
        GRAPH.addConnection(19, 18);
        GRAPH.addConnection(19, 20);
        GRAPH.addConnection(20, 19);
        GRAPH.addConnection(20, 21);
        GRAPH.addConnection(21, 20);
        GRAPH.addConnection(21, 22);
        GRAPH.addConnection(22, 21);
        GRAPH.addConnection(22, 23);
        GRAPH.addConnection(23, 22);
        GRAPH.addConnection(23, 24);
        GRAPH.addConnection(24, 23);
        GRAPH.addConnection(24, 25);
        GRAPH.addConnection(25, 24);
        //Connections Line 2
        GRAPH.addConnection(26, 370); // Ховрино (линия 2) и Ховрино (линия D3)
        GRAPH.addConnection(30, 288); // Войковская (линия 2) и Войковская (линия 14)
        GRAPH.addConnection(30, 287); // Войковская (линия 2) и Стрешнево (линия 14)
        GRAPH.addConnection(33, 224); // Динамо (линия 2) и Петровский парк (линия 11)
        GRAPH.addConnection(34, 96);  // Белорусская (линия 2) и Белорусская (линия 5)
        GRAPH.addConnection(34, 312); // Белорусская (линия 2) и Белорусская (линия D1)
        GRAPH.addConnection(34, 419); // Белорусская (линия 2) и Белорусская (линия D4)
        GRAPH.addConnection(36, 131); // Тверская (линия 2) и Пушкинская (линия 7)
        GRAPH.addConnection(36, 182); // Тверская (линия 2) и Чеховская (линия 9)
        GRAPH.addConnection(37, 58);  // Театральная (линия 2) и Площадь революции (линия 3)
        GRAPH.addConnection(37, 9);   // Театральная (линия 2) и Охотный ряд (линия 1)
        GRAPH.addConnection(38, 109); // Новокузнецкая (линия 2) и Третьяковская (линия 6)
        GRAPH.addConnection(38, 151); // Новокузнецкая (линия 2) и Третьяковская (линия 8)
        GRAPH.addConnection(39, 90);  // Павелецкая (линия 2) и Павелецкая (линия 5)
        GRAPH.addConnection(40, 275); // Автозаводская (линия 2) и Автозаводская (линия 14)
        GRAPH.addConnection(43, 237); // Каширская (линия 2) и Каширская (линия 11)
        GRAPH.addConnection(45, 353); // Царицыно (линия 2) и Царицыно (линия D2)
        GRAPH.addConnection(48, 222); // Красногвардейская (линия 2) и Зябликово (линия 10)
        GRAPH.addConnection(26, 27);
        GRAPH.addConnection(27, 26);
        GRAPH.addConnection(27, 28);
        GRAPH.addConnection(28, 27);
        GRAPH.addConnection(28, 29);
        GRAPH.addConnection(29, 28);
        GRAPH.addConnection(29, 30);
        GRAPH.addConnection(30, 29);
        GRAPH.addConnection(30, 31);
        GRAPH.addConnection(31, 30);
        GRAPH.addConnection(31, 32);
        GRAPH.addConnection(32, 31);
        GRAPH.addConnection(32, 33);
        GRAPH.addConnection(33, 32);
        GRAPH.addConnection(33, 34);
        GRAPH.addConnection(34, 33);
        GRAPH.addConnection(34, 35);
        GRAPH.addConnection(35, 34);
        GRAPH.addConnection(35, 36);
        GRAPH.addConnection(36, 35);
        GRAPH.addConnection(36, 37);
        GRAPH.addConnection(37, 36);
        GRAPH.addConnection(37, 38);
        GRAPH.addConnection(38, 37);
        GRAPH.addConnection(38, 39);
        GRAPH.addConnection(39, 38);
        GRAPH.addConnection(39, 40);
        GRAPH.addConnection(40, 39);
        GRAPH.addConnection(40, 41);
        GRAPH.addConnection(41, 40);
        GRAPH.addConnection(41, 42);
        GRAPH.addConnection(42, 41);
        GRAPH.addConnection(42, 43);
        GRAPH.addConnection(43, 42);
        GRAPH.addConnection(43, 44);
        GRAPH.addConnection(44, 43);
        GRAPH.addConnection(44, 45);
        GRAPH.addConnection(45, 44);
        GRAPH.addConnection(45, 46);
        GRAPH.addConnection(46, 45);
        GRAPH.addConnection(46, 47);
        GRAPH.addConnection(47, 46);
        GRAPH.addConnection(47, 48);
        GRAPH.addConnection(48, 47);
        GRAPH.addConnection(48, 49);
        GRAPH.addConnection(49, 48);

        //Connections Line 9
        GRAPH.addConnection(175, 262); // Владыкино (9) и Владыкино (14)
        GRAPH.addConnection(176, 374); // Петровско-Разумовская (9) и Петровско-Разумовская (D3)
        GRAPH.addConnection(176, 203); // Петровско-Разумовская (9) и Петровско-Разумовская (10)
        GRAPH.addConnection(177, 310); // Тимирязевская (9) и Тимирязевская (D1)
        GRAPH.addConnection(178, 337); // Дмитровская (9) и Дмитровская (D2)
        GRAPH.addConnection(179, 225); // Савёловская (9) и Савёловская (11)
        GRAPH.addConnection(179, 312); // Савёловская (9) и Савёловская (D1)
        GRAPH.addConnection(179, 420); // Савёловская (9) и Савёловская (D4)
        GRAPH.addConnection(180, 95);  // Менделеевская (9) и Новослободская (5)
        GRAPH.addConnection(181, 210); // Цветной бульвар (9) и Трубная (10)
        GRAPH.addConnection(182, 36);  // Чеховская (9) и Тверская (2)
        GRAPH.addConnection(182, 131); // Чеховская (9) и Пушкинская (7)
        GRAPH.addConnection(183, 59);  // Боровицкая (9) и Арбатская (3)
        GRAPH.addConnection(183, 10);  // Боровицкая (9) и Библиотека им. Ленина (1)
        GRAPH.addConnection(183, 82);  // Боровицкая (9) и Александровский сад (4)
        GRAPH.addConnection(185, 89);  // Серпуховская (9) и Добрынинская (5)
        GRAPH.addConnection(187, 277); // Нагатинская (9) и Верхние котлы (14)
        GRAPH.addConnection(190, 239); // Севастопольская (9) и Каховская (11)
        GRAPH.addConnection(196, 258); // Бульвар Дмитрия Донского (9) и Улица Старокачаловская (12)

        //Line 3
        GRAPH.addConnection(50, 51);
        GRAPH.addConnection(51, 50);
        GRAPH.addConnection(51, 52);
        GRAPH.addConnection(52, 51);
        GRAPH.addConnection(52, 53);
        GRAPH.addConnection(53, 52);
        GRAPH.addConnection(53, 54);
        GRAPH.addConnection(54, 53);
        GRAPH.addConnection(54, 55);
        GRAPH.addConnection(55, 54);
        GRAPH.addConnection(55, 56);
        GRAPH.addConnection(56, 55);
        GRAPH.addConnection(56, 57);
        GRAPH.addConnection(57, 56);
        GRAPH.addConnection(57, 58);
        GRAPH.addConnection(58, 57);
        GRAPH.addConnection(58, 59);
        GRAPH.addConnection(59, 58);
        GRAPH.addConnection(59, 60);
        GRAPH.addConnection(60, 59);
        GRAPH.addConnection(60, 61);
        GRAPH.addConnection(61, 60);
        GRAPH.addConnection(61, 62);
        GRAPH.addConnection(62, 61);
        GRAPH.addConnection(62, 63);
        GRAPH.addConnection(63, 62);
        GRAPH.addConnection(63, 64);
        GRAPH.addConnection(64, 63);
        GRAPH.addConnection(64, 65);
        GRAPH.addConnection(65, 64);
        GRAPH.addConnection(65, 66);
        GRAPH.addConnection(66, 65);
        GRAPH.addConnection(66, 67);
        GRAPH.addConnection(67, 66);
        GRAPH.addConnection(67, 68);
        GRAPH.addConnection(68, 67);
        GRAPH.addConnection(68, 69);
        GRAPH.addConnection(69, 68);
        GRAPH.addConnection(69, 70);
        GRAPH.addConnection(70, 69);
        GRAPH.addConnection(70, 71);
        GRAPH.addConnection(71, 70);


        //Line 4
        GRAPH.addConnection(72, 73);
        GRAPH.addConnection(73, 72);
        GRAPH.addConnection(73, 74);
        GRAPH.addConnection(74, 73);
        GRAPH.addConnection(74, 75);
        GRAPH.addConnection(75, 74);
        GRAPH.addConnection(75, 76);
        GRAPH.addConnection(76, 75);
        GRAPH.addConnection(76, 77);
        GRAPH.addConnection(77, 76);
        GRAPH.addConnection(77, 78);
        GRAPH.addConnection(78, 77);
        GRAPH.addConnection(78, 79);
        GRAPH.addConnection(79, 78);
        GRAPH.addConnection(79, 80);
        GRAPH.addConnection(80, 79);
        GRAPH.addConnection(80, 81);
        GRAPH.addConnection(81, 80);
        GRAPH.addConnection(81, 82);
        GRAPH.addConnection(82, 81);
        GRAPH.addConnection(83, 84);
        GRAPH.addConnection(84, 83);
        GRAPH.addConnection(84, 85);
        GRAPH.addConnection(85, 84);

        //Line 5
        GRAPH.addConnection(86, 87);
        GRAPH.addConnection(87, 86);
        GRAPH.addConnection(87, 88);
        GRAPH.addConnection(88, 87);
        GRAPH.addConnection(88, 89);
        GRAPH.addConnection(89, 88);
        GRAPH.addConnection(89, 90);
        GRAPH.addConnection(90, 89);
        GRAPH.addConnection(90, 91);
        GRAPH.addConnection(91, 90);
        GRAPH.addConnection(91, 92);
        GRAPH.addConnection(92, 91);
        GRAPH.addConnection(92, 93);
        GRAPH.addConnection(93, 92);
        GRAPH.addConnection(93, 94);
        GRAPH.addConnection(94, 93);
        GRAPH.addConnection(94, 95);
        GRAPH.addConnection(95, 94);
        GRAPH.addConnection(95, 96);
        GRAPH.addConnection(96, 95);
        GRAPH.addConnection(96, 97);
        GRAPH.addConnection(97, 96);

        // Пересадки Line 6
        GRAPH.addConnection(101, 263);
        GRAPH.addConnection(104, 227);
        GRAPH.addConnection(104, 339);
        GRAPH.addConnection(227, 339);
        GRAPH.addConnection(94, 105);
        GRAPH.addConnection(209, 107);
        GRAPH.addConnection(209, 7);
        GRAPH.addConnection(107, 7);
        GRAPH.addConnection(108, 133);
        GRAPH.addConnection(109, 151);
        GRAPH.addConnection(109, 38);
        GRAPH.addConnection(151, 38);
        GRAPH.addConnection(110, 88);
        GRAPH.addConnection(112, 279);
        GRAPH.addConnection(116, 241);
        GRAPH.addConnection(121, 260);
        // Line 6
        GRAPH.addConnection(98, 99);
        GRAPH.addConnection(99, 98);
        GRAPH.addConnection(99, 100);
        GRAPH.addConnection(100, 99);
        GRAPH.addConnection(100, 101);
        GRAPH.addConnection(101, 100);
        GRAPH.addConnection(101, 102);
        GRAPH.addConnection(102, 101);
        GRAPH.addConnection(102, 103);
        GRAPH.addConnection(103, 102);
        GRAPH.addConnection(103, 104);
        GRAPH.addConnection(104, 103);
        GRAPH.addConnection(104, 105);
        GRAPH.addConnection(105, 104);
        GRAPH.addConnection(105, 106);
        GRAPH.addConnection(106, 105);
        GRAPH.addConnection(106, 107);
        GRAPH.addConnection(107, 106);
        GRAPH.addConnection(107, 108);
        GRAPH.addConnection(108, 107);
        GRAPH.addConnection(108, 109);
        GRAPH.addConnection(109, 108);
        GRAPH.addConnection(109, 110);
        GRAPH.addConnection(110, 109);
        GRAPH.addConnection(110, 111);
        GRAPH.addConnection(111, 110);
        GRAPH.addConnection(111, 112);
        GRAPH.addConnection(112, 111);
        GRAPH.addConnection(112, 113);
        GRAPH.addConnection(113, 112);
        GRAPH.addConnection(113, 114);
        GRAPH.addConnection(114, 113);
        GRAPH.addConnection(114, 115);
        GRAPH.addConnection(115, 114);
        GRAPH.addConnection(115, 116);
        GRAPH.addConnection(116, 115);
        GRAPH.addConnection(116, 117);
        GRAPH.addConnection(117, 116);
        GRAPH.addConnection(117, 118);
        GRAPH.addConnection(118, 117);
        GRAPH.addConnection(118, 119);
        GRAPH.addConnection(119, 118);
        GRAPH.addConnection(119, 120);
        GRAPH.addConnection(120, 119);
        GRAPH.addConnection(120, 121);
        GRAPH.addConnection(121, 120);

        // Пересадки Line 7
        GRAPH.addConnection(124, 332);
        GRAPH.addConnection(125, 333);
        GRAPH.addConnection(126, 285);
        GRAPH.addConnection(126, 286);
        GRAPH.addConnection(285, 286);
        GRAPH.addConnection(127, 284);
        GRAPH.addConnection(127, 223);
        GRAPH.addConnection(127, 251);
        GRAPH.addConnection(284, 223);
        GRAPH.addConnection(284, 251);
        GRAPH.addConnection(223, 251);
        GRAPH.addConnection(131, 36);
        GRAPH.addConnection(131, 182);
        GRAPH.addConnection(36, 182);
        GRAPH.addConnection(132, 8);
        GRAPH.addConnection(108, 133);
        GRAPH.addConnection(134, 91);
        GRAPH.addConnection(134, 150);
        GRAPH.addConnection(91, 150);
        GRAPH.addConnection(135, 212);
        GRAPH.addConnection(136, 273);
        GRAPH.addConnection(137, 345);
        GRAPH.addConnection(137, 233);
        GRAPH.addConnection(345, 233);
        GRAPH.addConnection(139, 382);
        GRAPH.addConnection(140, 383);
        GRAPH.addConnection(141, 235);
        GRAPH.addConnection(141, 384);
        GRAPH.addConnection(235, 384);
        // Line 7
        GRAPH.addConnection(122, 123);
        GRAPH.addConnection(123, 122);
        GRAPH.addConnection(123, 124);
        GRAPH.addConnection(124, 123);
        GRAPH.addConnection(124, 125);
        GRAPH.addConnection(125, 124);
        GRAPH.addConnection(125, 126);
        GRAPH.addConnection(126, 125);
        GRAPH.addConnection(126, 127);
        GRAPH.addConnection(127, 126);
        GRAPH.addConnection(127, 128);
        GRAPH.addConnection(128, 127);
        GRAPH.addConnection(128, 129);
        GRAPH.addConnection(129, 128);
        GRAPH.addConnection(129, 130);
        GRAPH.addConnection(130, 129);
        GRAPH.addConnection(130, 131);
        GRAPH.addConnection(131, 130);
        GRAPH.addConnection(131, 132);
        GRAPH.addConnection(132, 131);
        GRAPH.addConnection(132, 133);
        GRAPH.addConnection(133, 132);
        GRAPH.addConnection(133, 134);
        GRAPH.addConnection(134, 133);
        GRAPH.addConnection(134, 135);
        GRAPH.addConnection(135, 134);
        GRAPH.addConnection(135, 136);
        GRAPH.addConnection(136, 135);
        GRAPH.addConnection(136, 137);
        GRAPH.addConnection(137, 136);
        GRAPH.addConnection(137, 138);
        GRAPH.addConnection(138, 137);
        GRAPH.addConnection(138, 139);
        GRAPH.addConnection(139, 138);
        GRAPH.addConnection(139, 140);
        GRAPH.addConnection(140, 139);
        GRAPH.addConnection(140, 141);
        GRAPH.addConnection(141, 140);
        GRAPH.addConnection(141, 142);
        GRAPH.addConnection(142, 141);
        GRAPH.addConnection(142, 143);
        GRAPH.addConnection(143, 142);

        // Пересадки Line 8, 8A
        GRAPH.addConnection(144, 429);
        GRAPH.addConnection(145, 428);
        GRAPH.addConnection(147, 436);
        GRAPH.addConnection(148, 231);
        GRAPH.addConnection(148, 378);
        GRAPH.addConnection(231, 378);
        GRAPH.addConnection(149, 211);
        GRAPH.addConnection(149, 424);
        GRAPH.addConnection(149, 342);
        GRAPH.addConnection(211, 424);
        GRAPH.addConnection(211, 342);
        GRAPH.addConnection(424, 342);
        GRAPH.addConnection(150, 91);
        GRAPH.addConnection(150, 134);
        GRAPH.addConnection(134, 91);
        GRAPH.addConnection(151, 109);
        GRAPH.addConnection(151, 38);
        GRAPH.addConnection(109, 38);
        GRAPH.addConnection(152, 253);
        GRAPH.addConnection(152, 84);
        GRAPH.addConnection(84, 253);
        GRAPH.addConnection(153, 62);
        GRAPH.addConnection(153, 416);
        GRAPH.addConnection(62, 416);
        GRAPH.addConnection(155, 415);
        GRAPH.addConnection(157, 244);
        // Lines 8 и 8А
        GRAPH.addConnection(144, 145);
        GRAPH.addConnection(145, 146);
        GRAPH.addConnection(146, 147);
        GRAPH.addConnection(147, 148);
        GRAPH.addConnection(148, 149);
        GRAPH.addConnection(149, 150);
        GRAPH.addConnection(150, 151);
        GRAPH.addConnection(152, 153);
        GRAPH.addConnection(153, 154);
        GRAPH.addConnection(154, 155);
        GRAPH.addConnection(155, 156);
        GRAPH.addConnection(156, 157);
        GRAPH.addConnection(157, 158);
        GRAPH.addConnection(158, 159);
        GRAPH.addConnection(159, 160);
        GRAPH.addConnection(160, 161);
        GRAPH.addConnection(161, 162);
        GRAPH.addConnection(162, 163);
        GRAPH.addConnection(163, 164);
        GRAPH.addConnection(164, 165);

        // Пересадки D1
        GRAPH.addConnection(306, 198);
        GRAPH.addConnection(309, 261);
        GRAPH.addConnection(309, 202);
        GRAPH.addConnection(261, 202);
        GRAPH.addConnection(310, 177);
        GRAPH.addConnection(311, 420);
        GRAPH.addConnection(311, 225);
        GRAPH.addConnection(311, 179);
        GRAPH.addConnection(420, 225);
        GRAPH.addConnection(420, 179);
        GRAPH.addConnection(225, 179);
        GRAPH.addConnection(312, 419);
        GRAPH.addConnection(312, 96);
        GRAPH.addConnection(312, 34);
        GRAPH.addConnection(419, 96);
        GRAPH.addConnection(419, 34);
        GRAPH.addConnection(96, 34);
        GRAPH.addConnection(313, 128);
        GRAPH.addConnection(314, 283);
        GRAPH.addConnection(314, 253);
        GRAPH.addConnection(283, 253);
        GRAPH.addConnection(315, 76);
        GRAPH.addConnection(316, 63);
        GRAPH.addConnection(317, 247);
        GRAPH.addConnection(317, 64);
        GRAPH.addConnection(317, 73);
        GRAPH.addConnection(247, 64);
        GRAPH.addConnection(247, 73);
        GRAPH.addConnection(64, 73);
        // Line D1
        GRAPH.addConnection(299, 300);
        GRAPH.addConnection(300, 299);
        GRAPH.addConnection(300, 301);
        GRAPH.addConnection(301, 300);
        GRAPH.addConnection(301, 302);
        GRAPH.addConnection(302, 301);
        GRAPH.addConnection(302, 303);
        GRAPH.addConnection(303, 302);
        GRAPH.addConnection(303, 304);
        GRAPH.addConnection(304, 303);
        GRAPH.addConnection(304, 305);
        GRAPH.addConnection(305, 304);
        GRAPH.addConnection(305, 306);
        GRAPH.addConnection(306, 305);
        GRAPH.addConnection(306, 307);
        GRAPH.addConnection(307, 306);
        GRAPH.addConnection(307, 308);
        GRAPH.addConnection(308, 307);
        GRAPH.addConnection(308, 309);
        GRAPH.addConnection(309, 308);
        GRAPH.addConnection(309, 310);
        GRAPH.addConnection(310, 309);
        GRAPH.addConnection(310, 311);
        GRAPH.addConnection(311, 310);
        GRAPH.addConnection(311, 312);
        GRAPH.addConnection(312, 311);
        GRAPH.addConnection(312, 313);
        GRAPH.addConnection(313, 312);
        GRAPH.addConnection(313, 314);
        GRAPH.addConnection(314, 313);
        GRAPH.addConnection(314, 315);
        GRAPH.addConnection(315, 314);
        GRAPH.addConnection(315, 316);
        GRAPH.addConnection(316, 315);
        GRAPH.addConnection(316, 317);
        GRAPH.addConnection(317, 316);
        GRAPH.addConnection(317, 318);
        GRAPH.addConnection(318, 317);
        GRAPH.addConnection(318, 319);
        GRAPH.addConnection(319, 318);
        GRAPH.addConnection(319, 320);
        GRAPH.addConnection(320, 319);
        GRAPH.addConnection(320, 321);
        GRAPH.addConnection(321, 320);
        GRAPH.addConnection(321, 322);
        GRAPH.addConnection(322, 321);
        GRAPH.addConnection(322, 323);
        GRAPH.addConnection(323, 322);

        // Пересадки D2
        GRAPH.addConnection(333, 125);
        GRAPH.addConnection(334, 287);
        GRAPH.addConnection(334, 30);
        GRAPH.addConnection(334, 287);
        GRAPH.addConnection(337, 178);
        GRAPH.addConnection(338, 421);
        GRAPH.addConnection(338, 206);
        GRAPH.addConnection(338, 421);
        GRAPH.addConnection(339, 227);
        GRAPH.addConnection(339, 102);
        GRAPH.addConnection(340, 422);
        GRAPH.addConnection(340, 5);
        GRAPH.addConnection(340, 93);
        GRAPH.addConnection(341, 282);
        GRAPH.addConnection(341, 62);
        GRAPH.addConnection(341, 92);
        GRAPH.addConnection(341, 210);
        GRAPH.addConnection(344, 272);
        GRAPH.addConnection(345, 137);
        GRAPH.addConnection(345, 233);
        GRAPH.addConnection(346, 235);
        GRAPH.addConnection(346, 236);
        GRAPH.addConnection(353, 45);
        // Line D2
        GRAPH.addConnection(324, 325);
        GRAPH.addConnection(325, 326);
        GRAPH.addConnection(326, 327);
        GRAPH.addConnection(327, 328);
        GRAPH.addConnection(328, 329);
        GRAPH.addConnection(329, 330);
        GRAPH.addConnection(330, 331);
        GRAPH.addConnection(331, 332);
        GRAPH.addConnection(332, 333);
        GRAPH.addConnection(333, 334);
        GRAPH.addConnection(334, 335);
        GRAPH.addConnection(335, 336);
        GRAPH.addConnection(336, 337);
        GRAPH.addConnection(337, 338);
        GRAPH.addConnection(338, 339);
        GRAPH.addConnection(339, 340);
        GRAPH.addConnection(340, 341);
        GRAPH.addConnection(341, 342);
        GRAPH.addConnection(342, 343);
        GRAPH.addConnection(343, 344);
        GRAPH.addConnection(344, 345);
        GRAPH.addConnection(345, 346);
        GRAPH.addConnection(346, 347);
        GRAPH.addConnection(347, 348);
        GRAPH.addConnection(348, 349);
        GRAPH.addConnection(349, 350);
        GRAPH.addConnection(350, 351);
        GRAPH.addConnection(351, 352);
        GRAPH.addConnection(352, 353);
        GRAPH.addConnection(353, 354);
        GRAPH.addConnection(354, 355);
        GRAPH.addConnection(355, 356);
        GRAPH.addConnection(356, 357);
        GRAPH.addConnection(357, 358);
        GRAPH.addConnection(358, 359);
        GRAPH.addConnection(359, 360);
        GRAPH.addConnection(360, 361);
        //Line 11
        GRAPH.addConnection(229, 55);  // Электрозаводская (11) и Электрозаводская (3)
        GRAPH.addConnection(229, 376); // Электрозаводская (11) и Электрозаводская (D3)
        GRAPH.addConnection(228, 3);   // Сокольники (11) и Сокольники (1)
        GRAPH.addConnection(227, 104); // Рижская (11) и Рижская (6)
        GRAPH.addConnection(227, 339); // Рижская (11) и Рижская (D2)
        GRAPH.addConnection(226, 206); // Марьина Роща (11) и Марьина Роща (10)
        GRAPH.addConnection(226, 338); // Марьина Роща (11) и Марьина Роща (D2)
        GRAPH.addConnection(226, 421); // Марьина Роща (11) и Марьина Роща (D4)
        GRAPH.addConnection(225, 179); // Савёловская (11) и Савёловская (9)
        GRAPH.addConnection(225, 312); // Савёловская (11) и Савёловская (D1)
        GRAPH.addConnection(225, 420); // Савёловская (11) и Савёловская (D4)
        GRAPH.addConnection(224, 33);  // Петровский парк (11) и Динамо (2)
        GRAPH.addConnection(223, 127); // Хорошёвская (11) и Полежаевская (7)
        GRAPH.addConnection(223, 284); // Хорошёвская (11) и Хорошёво (14)
        GRAPH.addConnection(223, 251); // Хорошёвская (11) и Хорошёвская (11A)
        GRAPH.addConnection(247, 317); // Кунцевская (11) и Кунцевская (D1)
        GRAPH.addConnection(247, 72);  // Кунцевская (11) и Кунцевская (3)
        GRAPH.addConnection(247, 73);  // Кунцевская (11) и Кунцевская (4)
        GRAPH.addConnection(245, 413); // Аминьевская (11) и Аминьевская (D4)
        GRAPH.addConnection(244, 158); // Мичуринский проспект (11) и Мичуринский проспект (8A)
        GRAPH.addConnection(243, 17);  // Проспект Вернадского (11) и Проспект Вернадского (1)
        GRAPH.addConnection(241, 116); // Воронцовская (11) и Калужская (6)
        GRAPH.addConnection(239, 190); // Каховская (11) и Севастопольская (9)
        GRAPH.addConnection(237, 43);  // Каширская (11) и Каширская (2)
        GRAPH.addConnection(234, 346); // Печатники (11) и Печатники (D2)
        GRAPH.addConnection(234, 215); // Печатники (11) и Печатники (10)
        GRAPH.addConnection(233, 138); // Текстильщики (11) и Текстильщики (7)
        GRAPH.addConnection(233, 347); // Текстильщики (11) и Текстильщики (D2)
        GRAPH.addConnection(232, 425); // Нижегородская (11) и Нижегородская (D4)
        GRAPH.addConnection(232, 272); // Нижегородская (11) и Нижегородская (14)
        //Line 14
        GRAPH.addConnection(262, 175); // Владыкино (14) и Владыкино (9)
        GRAPH.addConnection(261, 202); // Окружная (14) и Окружная (10)
        GRAPH.addConnection(261, 309); // Окружная (14) и Окружная (D1)
        GRAPH.addConnection(373, 373); // Лихоборы (14) и Лихоборы (D3)
        GRAPH.addConnection(288, 30);  // Балтийская (14) и Войковская (2)
        GRAPH.addConnection(287, 30);  // Стрешнево (14) и Войковская (2)
        GRAPH.addConnection(287, 334); // Стрешнево (14) и Стрешнево (D2)
        GRAPH.addConnection(286, 126); // Панфиловская (14) и Октябрьское поле (7)
        GRAPH.addConnection(285, 126); // Зорге (14) и Октябрьское поле (7)
        GRAPH.addConnection(284, 223); // Хорошёво (14) и Хорошёвская (11)
        GRAPH.addConnection(284, 251); // Хорошёво (14) и Хорошёвская (11A)
        GRAPH.addConnection(284, 127); // Хорошёво (14) и Полежаевская (7)
        GRAPH.addConnection(283, 252); // Шелепиха (14) и Шелепиха (11A)
        GRAPH.addConnection(283, 314); // Шелепиха (14) и Тестовская (D1)
        GRAPH.addConnection(282, 418); // Деловой центр МЦК (14) и Тестовская (D4)
        GRAPH.addConnection(282, 84);  // Деловой центр МЦК (14) и Международная (4A)
        GRAPH.addConnection(281, 417); // Кутузовская (14) и Кутузовская (D4)
        GRAPH.addConnection(281, 77);  // Кутузовская (14) и Кутузовская (4)
        GRAPH.addConnection(280, 14);  // Лужники (14) и Спортивная (1)
        GRAPH.addConnection(279, 112); // Площадь Гагарина (14) и Ленинский проспект (6)
        GRAPH.addConnection(278, 187); // Верхние Котлы (14) и Нагатинская (9)
        GRAPH.addConnection(275, 40);  // Автозаводская (14) и Автозаводская (2)
        GRAPH.addConnection(274, 213); // Дубровка (14) и Дубровка (10)
        GRAPH.addConnection(274, 214); // Дубровка (14) и Кожуховская (10)
        GRAPH.addConnection(273, 136); // Угрешская (14) и Волгоградский проспект (7)
        GRAPH.addConnection(272, 344); // Новохохловская (14) и Новохохловская (D2)
        GRAPH.addConnection(271, 379); // Андроновка (14) и Андроновка (D3)
        GRAPH.addConnection(270, 147); // Шоссе Энтузиастов (14) и Шоссе Энтузиастов (8)
        GRAPH.addConnection(268, 53);  // Измайлово (14) и Партизанская (3)
        GRAPH.addConnection(267, 1);   // Локомотив (14) и Черкизовская (1)
        GRAPH.addConnection(266, 0);   // Бульв
        GRAPH.addConnection(229, 230);   // Бульв
        GRAPH.addConnection(230, 231);   // Бульв
        // D3
        GRAPH.addConnection(370, 26); // Ховрино (D3) и Ховрино (2)
        GRAPH.addConnection(373, 373); // Лихоборы (D3) и Лихоборы (14)
        GRAPH.addConnection(374, 203); // Петровско-Разумовская (D3) и Петровско-Разумовская (10)
        GRAPH.addConnection(374, 176); // Петровско-Разумовская (D3) и Петровско-Разумовская (9)
        GRAPH.addConnection(375, 205); // Останкино (D3) и Бутырская (10)
        GRAPH.addConnection(376, 229); // Электрозаводская (D3) и Электрозаводская (11)
        GRAPH.addConnection(376, 55);  // Электрозаводская (D3) и Электрозаводская (3)
        GRAPH.addConnection(378, 231); // Авиамоторная (D3) и Авиамоторная (11)
        GRAPH.addConnection(378, 148); // Авиамоторная (D3) и Авиамоторная (8)
        GRAPH.addConnection(379, 271); // Андроновка (D3) и Андроновка (14)
        GRAPH.addConnection(380, 426); // Перово (D3) и Чухлинка (D4)
        GRAPH.addConnection(383, 140); // Выхино (D3) и Выхино (7)
        GRAPH.addConnection(384, 296); // Косино (D3) и Косино (15)
        GRAPH.addConnection(384, 141); // Косино (D3) и Лермонтовский проспект (7)
        //D4
        GRAPH.addConnection(410, 405); // Солнечная (D4) и Солнечная (D4A)
        GRAPH.addConnection(413, 245); // Аминьевская (D4) и Аминьевская (11)
        GRAPH.addConnection(415, 85);  // Минская (D4) и Минская (8A)
        GRAPH.addConnection(416, 84);  // Поклонная (D4) и Поклонная (8A)
        GRAPH.addConnection(416, 62);  // Поклонная (D4) и Поклонная (3)
        GRAPH.addConnection(417, 77);  // Кутузовская (D4) и Кутузовская (4)
        GRAPH.addConnection(417, 281); // Кутузовская (D4) и Кутузовская (14)
        GRAPH.addConnection(418, 282); // Тестовская (D4) и Деловой центр МЦК (14)
        GRAPH.addConnection(418, 84);  // Тестовская (D4) и Международная (4A)
        GRAPH.addConnection(419, 34);  // Белорусская (D4) и Белорусская (2)
        GRAPH.addConnection(419, 96);  // Белорусская (D4) и Белорусская (5)
        GRAPH.addConnection(419, 312); // Белорусская (D4) и Белорусская (D1)
        GRAPH.addConnection(420, 179); // Савёловская (D4) и Савёловская (9)
        GRAPH.addConnection(420, 312); // Савёловская (D4) и Савёловская (D1)
        GRAPH.addConnection(420, 225); // Савёловская (D4) и Савёловская (11)
        GRAPH.addConnection(421, 206); // Марьина Роща (D4) и Марьина Роща (10)
        GRAPH.addConnection(421, 338); // Марьина Роща (D4) и Марьина Роща (D2)
        GRAPH.addConnection(421, 226); // Марьина Роща (D4) и Марьина Роща (11)
        GRAPH.addConnection(422, 340); // Пл. трёх вокзалов (D4) и Пл. трёх вокзалов (D2)
        GRAPH.addConnection(423, 57);  // Курская (D4) и Курская (3)
        GRAPH.addConnection(423, 92);  // Курская (D4) и Курская (5)
        GRAPH.addConnection(423, 342); // Курская (D4) и Курская (D2)
        GRAPH.addConnection(423, 210); // Курская (D4) и Чкаловская (10)
        GRAPH.addConnection(424, 211); // Серп и Молот (D4) и Римская (10)
        GRAPH.addConnection(424, 149); // Серп и Молот (D4) и Площадь Ильича (8)
        GRAPH.addConnection(424, 343); // Серп и Молот (D4) и Москва-Товарная (D2)
        GRAPH.addConnection(425, 232); // Нижегородская (D4) и Нижегородская (11)
        GRAPH.addConnection(425, 272); // Нижегородская (D4) и Нижегородская (14)
        GRAPH.addConnection(425, 291); // Нижегородская (D4) и Нижегородская (15)
        GRAPH.addConnection(426, 380); // Чухлинка (D4) и Перово (D3)
        GRAPH.addConnection(428, 145); // Новогиреево (D4) и Новогиреев

    }

    private Constant() {
    }
}
